# Contributor: Jake Buchholz Göktürk <tomalok@gmail.com>
# Maintainer: Jake Buchholz Göktürk <tomalok@gmail.com>
pkgname=docker-cli-buildx
pkgver=0.15.0
pkgrel=0
_commit=d3a53189f7e9c917eeff851c895b9aad5a66b108
pkgdesc="A Docker CLI plugin for extended build capabilities"
url="https://docs.docker.com/engine/reference/commandline/buildx_build"
arch="all"
license="Apache-2.0"
makedepends="go"
options="net"
source="buildx-$pkgver.tar.gz::https://github.com/docker/buildx/archive/v$pkgver.tar.gz"

_buildx_installdir="/usr/libexec/docker/cli-plugins"

builddir="$srcdir"/buildx-"$pkgver"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	PKG=github.com/docker/buildx
	local ldflags="-X $PKG/version.Version=v$pkgver -X $PKG/version.Revision=$_commit -X $PKG/version.Package=$PKG"
	go build -v -modcacherw -ldflags "$ldflags" -o docker-buildx ./cmd/buildx
}

check() {
	# bake and gitutil tests do not succeed inside abuild environment
	local pkgs="$(go list -modcacherw ./... | grep -Ev '(bake|gitutil)')"
	go test -modcacherw -short $pkgs
	./docker-buildx version
}

package() {
	# this is circular to have top-level, so depend on it in package itself
	depends="docker-cli"
	install -Dm755 docker-buildx "$pkgdir$_buildx_installdir"/docker-buildx
}

sha512sums="
f11ae2ed5e45728daa9d48dd14b64b940c84a0eac7256cd7dda7f84b2c6c59c6fc8a70a907a3cd2882cf0c1581cd52f24c28df70492b696f743eadaba7ade179  buildx-0.15.0.tar.gz
"
